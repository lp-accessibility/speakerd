# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd (at) circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

from setuptools import setup, find_packages

import sys, os, io
with io.open(os.path.join(os.path.dirname(sys.argv[0]), 'speakerd', 'version.py')) as infh:
    exec(infh.read())

setup(
    name = "speakerd",
    packages = find_packages(exclude=["tests"]),
    entry_points = {
        "console_scripts": ['speakerd = speakerd:main']
        },
    install_requires=[ 'xdg', 'pyyaml' ],
    test_suite="tests",
    version = __version__,
    description = "A text to speech daemon based on espeak.",
    long_description = "Service supporting speech-dispatcher SSIP protocol. It is a drop-in replacement for speech-dispatcher but that can only use espeak as output module.",
    author = "Laurent Pelecq",
    author_email = "lpelecq+speakerd@circoise.eu",
    license='GNU General Public License version 3'
    )
