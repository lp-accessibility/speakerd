# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import hashlib
import io
import os
import time

__all__ = [ 'DirectOutputCache', 'DelayedOutputCache' ]

def hasher(data):
    m = hashlib.sha1()
    m.update(data)
    return m.hexdigest()

def touch(filename, mode=0o666, timestamp=None):
    flags = os.O_CREAT | os.O_APPEND
    now = timestamp or time.time()
    if os.utime in os.supports_fd:
        with os.fdopen(os.open(filename, flags=flags, mode=mode)) as fh:
            os.utime(fh.fileno(), times=(now, now))
    else:
        os.utime(filename, times=(now, now))


class DirectCachedItem:

    """Store the information for a cached output."""

    def __init__(self, name, path, stat_result):
        self.name = name
        self.path = path
        self.__update(stat_result)

    def __update(self, stat_result):
        if stat_result:
            self.timestamp = stat_result.st_mtime
            self.size = stat_result.st_size
        else:
            self.timestamp = None
            self.size = 0

    def update(self):
        """Update properties."""
        try:
            stat_result = os.lstat(self.path)
            self.__update(stat_result)
        except FileNotFoundError:
            self.timestamp = None
            self.size = 0

    def exists(self):
        """The output exists on disk."""
        return self.timestamp != None

    def is_cacheable(self):
        """The output doesn't exists and can be created."""
        return not self.exists()

    def touch(self, timestamp, is_new):
        """Update the timestamp on disk."""
        if self.exists():
            touch(self.path, timestamp=timestamp)
            self.timestamp = timestamp

    def __repr__(self):
        return str({ 'path': self.path, 'timestamp': self.timestamp })

class DelayedCachedItem(DirectCachedItem):

    """Store the information for a cached output.

    If size is zero, the output hasn't been saved yet. If timestamp is None, it is the first time
    the data is requested."""

    def exists(self):
        """The output exists on disk."""
        return self.timestamp != None and self.size > 0

    def is_cacheable(self):
        """The output doesn't exists but is a candidate to be cached."""
        return self.timestamp != None and self.size == 0

    def touch(self, timestamp, is_new):
        """Update the timestamp on disk or create an empty file."""
        touch(self.path, timestamp=timestamp)
        if is_new or self.timestamp:
            self.timestamp = timestamp


class BaseOutputCache:

    """Cache manager for binary output.

    Returns cache items where the data can be stored."""

    def __init__(self, cache_dir, max_entries, item_factory):
        self.cache_dir = cache_dir
        self.max_entries = max_entries
        self.item_factory = item_factory
        self.entries = {}
        for entry in os.scandir(self.cache_dir):
            if entry.is_file():
                self.entries[entry.name] = self.item_factory(entry.name, entry.path, entry.stat(follow_symlinks=False))

    def cleanup(self):
        files = sorted([ entry for entry in self.entries.values() if entry.timestamp ], key=lambda entry: entry.timestamp)
        while len(files) > self.max_entries:
            entry = files.pop(0)
            del self.entries[entry.name]
            os.unlink(entry.path)

    def get(self, input_data):
        """Return a CachedItem for the input data"""
        key = hasher(input_data)
        now = time.time()
        is_new = False
        if key in self.entries:
            entry = self.entries[key]
            is_new = True
        else:
            filename = os.path.join(self.cache_dir, key)
            self.entries[key] = entry = self.item_factory(key, filename, None)
        entry.touch(now, is_new)
        return entry


class DirectOutputCache(BaseOutputCache):

    def __init__(self, cache_dir, max_entries):
        super().__init__(cache_dir, max_entries, DirectCachedItem)


class DelayedOutputCache(BaseOutputCache):

    def __init__(self, cache_dir, max_entries):
        super().__init__(cache_dir, max_entries, DelayedCachedItem)
