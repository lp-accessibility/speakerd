# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

__all__ = [ 'available_outputs', 'available_voices', 'find_command', 'VoiceNotFoundError' ]

import importlib

from .base import find_command
from .voices import VoiceManager, VoiceNotFoundError

def available_outputs(config):
    result = {}
    for name in ['dummy', 'espeak', 'pico']:
        try:
            mod = importlib.import_module('.{}'.format(name), __name__)
            factory = getattr(mod, 'PlayBack')
            playback = factory(config, VoiceManager())
            if playback.is_available():
                result[name] = playback
        except ImportError:
            pass
    return result

def available_voices():
    result = {}
    for name in ['dummy', 'espeak', 'pico']:
        try:
            mod = importlib.import_module('.{}'.format(name), __name__)
            if hasattr(mod, 'Voices'):
                factory = getattr(mod, 'Voices')
                voice_manager = VoiceManager()
                voice_manager.add_voices(factory().load())
                result[name] = voice_manager.voices_by_language()
        except ImportError:
            pass
    return result
