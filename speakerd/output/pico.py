# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import logging
import os
import subprocess

__all__ = []

from .base import PlayBackWithCache, find_command
from .cache import DirectOutputCache
from .voices import Voice

class Voices:

    SUFFIX = '_ta.bin'

    def __init__(self, config=None):
        self.voices = []
        lang_dir = config.get('modules', 'pico', 'lang_dir', defval=None) if config else None
        self.lang_dir = lang_dir or '/usr/share/pico/lang'

    def load(self):
        """Load the list of languages supported by pico."""
        if not self.voices and os.path.isdir(self.lang_dir):
            for filename in os.listdir(self.lang_dir):
                if filename.endswith(self.SUFFIX):
                    voice_name = filename[0:-len(self.SUFFIX)]
                    voice = Voice(voice_name, 'F')
                    language = voice.name.split('_')[0].replace('-', '_')
                    voice.add_language(language, 1)
                    self.voices.append(voice)
        return self.voices

class PlayBack(PlayBackWithCache):

    NAME = 'pico'

    def __init__(self, config, voice_manager):
        super().__init__(config, self.NAME, DirectOutputCache)
        self.pico_command = config.get('modules', self.NAME, 'commands', 'pico2wave', defval=find_command('pico2wave'))
        self.voice_manager = voice_manager
        voice_config = config.get('modules', self.NAME, 'voices')
        if voice_config:
            self.voice_manager.initialize(voice_config)
        else:
            self.voice_manager.add_voices(Voices(config).load())

    def is_available(self):
        return self.pico_command != None

    def produce(self, msg, output_path):
        """Produce the output."""
        self.spawn_process([ self.pico_command, '-l', msg.parameters.physical_voice, '-w', output_path, msg.data ])
