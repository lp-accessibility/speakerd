# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import argparse
import io
import locale
import logging
import logging.config
import os
import shutil
import sys
import tempfile
import xdg.BaseDirectory

import speakerd.config
from .version import __version__
from .server import *
from .client import *

### Configuration

class ApplicationEnvironment:

    """Application configuration following the XDG base directory specification.

    See: http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html"""

    def __init__(self, application_name, config_dir=None, cache_dir=None, runtime_dir=None):
        self.application_name = application_name
        self.config_dir = config_dir or xdg.BaseDirectory.load_first_config(application_name)
        self.cache_dir = cache_dir
        self.runtime_dir = runtime_dir
        self.temp_dir = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.temp_dir:
            temp_dir = self.temp_dir
            self.temp_dir = None
            shutil.rmtree(temp_dir)

    def __make_sub_directory(self, root_dir, name):
        dirname = os.path.join(root_dir, name)
        if not os.path.isdir(dirname):
            os.mkdir(dirname)
        return dirname

    def get_config_dir(self, basename=None):
        """Returns the configuration directory, creates it if necessary."""
        if basename:
            return xdg.BaseDirectory.save_config_path(basename)
        elif not self.config_dir:
            self.config_dir = xdg.BaseDirectory.save_config_path(self.application_name)
        return self.config_dir

    def get_cache_dir(self, basename=None):
        """Returns the permanent cache directory, creates it if necessary."""
        if basename:
            return xdg.BaseDirectory.save_cache_path(basename)
        elif not self.cache_dir:
            self.cache_dir = xdg.BaseDirectory.save_cache_path(self.application_name)
        return self.cache_dir

    def get_runtime_dir(self, basename=None):
        """Returns the temporary runtime directory, creates it if necessary."""
        runtime_dir = xdg.BaseDirectory.get_runtime_dir()
        if basename:
            return self.__make_sub_directory(runtime_dir, basename)
        elif not self.runtime_dir:
            self.runtime_dir = self.__make_sub_directory(runtime_dir, self.application_name)
        return self.runtime_dir

    def get_temp_dir(self):
        """Returns the temporary runtime directory, creates it if necessary."""
        if not self.temp_dir:
            self.temp_dir = tempfile.mkdtemp(dir=self.get_runtime_dir())
        return self.temp_dir

class LoggingConfiguration:

    """Initialize the logging subsystem.

    Search for a configuration file in YAML, JSON or INI-like format."""

    default_logging_config = {
        'version': 1,
        'root': {
            'level': 'WARNING',
            'handlers': ['console']
        },
        'formatters': {
            'simple': { 'format': '%(levelname)s: %(message)s' },
            "timestamp": { "format": "%(asctime)s %(levelname)s: %(message)s" }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'formatter': 'simple'
            },
            "file": {
                "maxBytes": 10000,
                "formatter": "timestamp",
                "class": "logging.handlers.RotatingFileHandler",
                "backupCount": 5,
                "filename": "/var/log/{}.log".format(__name__)
            }
        }
    }

    NATIVE_EXTENSION = 'conf'

    LEVELS = {
        logging.getLevelName(logging.DEBUG).lower(): logging.DEBUG,
        logging.getLevelName(logging.INFO).lower(): logging.INFO,
        logging.getLevelName(logging.WARNING).lower(): logging.WARNING,
        logging.getLevelName(logging.ERROR).lower(): logging.ERROR
    }

    def __init__(self, config_dir):
        extensions = config.supported_extensions() + [ self.NATIVE_EXTENSION ]
        self.config_file = default_config_file = None
        for ext in extensions:
            config_file = os.path.join(config_dir, 'logging.{}'.format(ext))
            if not default_config_file:
                default_config_file = config_file
            if os.path.isfile(config_file):
                self.config_file = config_file
        if not self.config_file:
            self.config_file = default_config_file

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        logging.shutdown()

    def has_config_file(self):
        return os.path.isfile(self.config_file)

    def write_default_config(self, log_file_name, max_file_length=None):
        cfg = self.default_logging_config
        cfg['handlers']['file']['filename'] = log_file_name
        if max_file_length:
            cfg['handlers']['file']['maxBytes'] = max_file_length
        config_file = config.get_file(self.config_file, data=cfg)
        config_file.save()

    def initialize(self, level=None):
        if self.config_file.endswith('.{}'.format(self.NATIVE_EXTENSION)):
            logging.config.fileConfig(self.config_file)
        else:
            config_file = config.get_file(self.config_file)
            cfg = config_file.load()
            logging.config.dictConfig(cfg.as_dict())
        if level:
            root = logging.getLogger()
            root.setLevel(self.LEVELS[level.lower()])

### Parsing command line

def parse_command_line(argv=None):
    argparser = argparse.ArgumentParser(description="A text to speech daemon based on command line TTS engines.")

    argparser.add_argument('--debug', action="store_true", default=False,
                           help="Debug mode, for developers only")

    argparser.add_argument("-v", "--verbose",
                           dest="verbose", action="store_true", default=False,
                           help="print status messages to stdout")

    argparser.add_argument("-c", "--config-dir",
                           dest="config_dir", action="store", default=None,
                           metavar="FOLDER", help="Configuration directory.")

    argparser.add_argument("--cache-dir",
                           dest="cache_dir", action="store", default=None,
                           metavar="FOLDER", help="Cache directory.")

    argparser.add_argument("--runtime-dir",
                           dest="runtime_dir", action="store", default=None,
                           metavar="FOLDER", help="Working directory.")

    argparser.add_argument("--socket-path",
                           dest="socket_path", action="store", default=None,
                           metavar="FILE", help="Socket to communicate with clients.")

    argparser.add_argument("--pitch", type=int,
                           dest="pitch", action="store", default=ClientHandler.PARAMETERS.DEFAULT_PITCH,
                           metavar="VALUE",
                           help="Default pitch (in range [{},{}], default {}).".format(ClientHandler.PARAMETERS.MIN_PITCH, ClientHandler.PARAMETERS.MAX_PITCH, ClientHandler.PARAMETERS.DEFAULT_PITCH))

    argparser.add_argument("--rate", type=int,
                           dest="rate", action="store", default=ClientHandler.PARAMETERS.DEFAULT_RATE,
                           metavar="VALUE",
                           help="Default rate (in range [{},{}], default {}).".format(ClientHandler.PARAMETERS.MIN_RATE, ClientHandler.PARAMETERS.MAX_RATE, ClientHandler.PARAMETERS.DEFAULT_RATE))

    argparser.add_argument("--voices",
                           dest="list_voices", action="store", default='none', nargs='?',
                           metavar="LANGUAGE", help="List voices for languages.")

    argparser.add_argument("--exit", action="store_true", help="Generate configuration and exit.")

    argparser.add_argument('--version', action='version', version='%(prog)s {}'.format(__version__))

    return argparser.parse_args(args=argv)

### Main

def create_containing_folder(filename):
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname):
        os.mkdir(dirname)

def print_voices(voices, output):
    titles = [ 'Language', 'Logical name', 'Name' ]
    output.write('{}\n'.format(' '.join(titles)))
    output.write('{}\n'.format(' '.join([ '-'*len(x) for x in titles ])))
    lang_width = len(titles[0])
    name_width = len(titles[1])
    for language in voices:
        language_title = language[0:lang_width]
        voices_subset = voices[language]
        for logical_name in voices_subset:
            voice = voices_subset[logical_name]
            output.write('{1: <{0}} {3: <{2}} {4}\n'.format(lang_width, language_title, name_width, logical_name, voice.name))
            language_title = ''

def run(argv):
    args = parse_command_line(argv)
    locale.setlocale(locale.LC_ALL, '')
    with ApplicationEnvironment(__name__, args.config_dir, args.cache_dir, args.runtime_dir) as appenv:
        config_file = config.find_file(os.path.join(appenv.get_config_dir(), 'main'))
        socket_path = args.socket_path
        if not config_file.exists():
            if not socket_path:
                socket_path = os.path.join(appenv.get_runtime_dir('speech-dispatcher'), 'speechd.sock')
            config_file.set_default('server', 'socket_path', socket_path)
            config_file.set_default('server', 'max_clients', 5)
            config_file.set_default('client', 'commands', 'play', find_command('aplay'))
            config_file.set_default('client', 'cache', 'max_entries', 100)
            config_file.set_default('client', 'cache', 'directory', appenv.get_cache_dir())
            config_file.set_default('client', 'output', 'module', 'espeak')
            config_file.set_default('client', 'output', 'pitch', args.pitch)
            config_file.set_default('client', 'output', 'rate', args.rate)
            voices_by_module = ClientHandler.detect_voices()
            for module_name in voices_by_module:
                voices_by_language = voices_by_module[module_name]
                for language in voices_by_language:
                    voices_by_name = voices_by_language[language]
                    for name in voices_by_name:
                        config_file.set_default('modules', module_name, 'voices', language, name, voices_by_name[name])
            cfg = config_file.save()
        else:
            cfg = config_file.load()
            if not socket_path:
                socket_path = cfg.get('server', 'socket_path')

        if socket_path:
            create_containing_folder(socket_path)

        log_level = None
        if args.debug:
            log_level = 'DEBUG'
        with LoggingConfiguration(appenv.get_config_dir()) as log_config:
            if not log_config.has_config_file():
                log_config.write_default_config(os.path.join(appenv.get_runtime_dir(), '{}.log'.format(appenv.application_name)))
            log_config.initialize(log_level)
            if args.list_voices != 'none':
                print_voices(get_voices(args.list_voices), sys.stdout)
                return 0
            elif args.exit:
                return 0

            logging.debug("debug mode enabled")
            with ClientHandler(cfg.get('client')) as client_handler:
                with Server(cfg.get('server'), client_handler) as server:
                    server.run()
    return 0

def main(argv=None):
    exit_code = 1
    try:
        exit_code = run(argv)
    except Exception as ex:
        logging.error('fatal error: {}'.format(ex))
        logging.debug('fatal error', exc_info=sys.exc_info())
    sys.exit(exit_code)
