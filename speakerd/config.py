# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import collections
import io
import os

default_extension = None

backends = collections.OrderedDict()

class ConfigData:

    def __init__(self, data=None):
        self.data = data or {}

    def __set_node(self, node, path, value):
        key = path.pop(0)
        if not path:
            node[key] = value
        else:
            try:
                subnode = node[key]
            except KeyError:
                subnode = node[key] = {}
            self.__set_node(subnode, path, value)

    def __get_node(self, node, path, defval):
        key = path.pop(0)
        if key == None:
            raise ValueError('Null configuration key')
        try:
            subnode = node[key]
            if not path:
                return subnode
            else:
                return self.__get_node(subnode, path, defval)
        except KeyError:
            return defval

    def set(self, *args):
        path = list(args)
        value = path.pop()
        self.__set_node(self.data, path, value)

    def get(self, *args, **kw):
        path = list(args)
        defval = None
        if 'defval' in kw:
            defval = kw['defval']
        value = self.__get_node(self.data, path, defval)
        if isinstance(value, dict):
            return ConfigData(value)
        return value

    def as_dict(self):
        return self.data

class Backend:

    def __init__(self, filename, data=None):
        self.filename = filename
        self.config = ConfigData(data)

    def set_default(self, *args):
        self.config.set(*args)

    def exists(self):
        return os.path.isfile(self.filename)

    def load(self):
        if os.path.isfile(self.filename):
            with io.open(self.filename) as fh:
                self.config = ConfigData(self.load_stream(fh))
        return self.config

    def save(self):
        with io.open(self.filename, 'w') as fh:
            self.dump_stream(fh, self.config.data)
        return self.config

def get_file(filename, data=None, extension=None):
    """Get the configuration file with name FILENAME.

DATA is the initial configuration. If EXTENSION is omitted, it depends on the file extension."""
    if not extension:
        name, file_extension = os.path.splitext(filename)
        if not file_extension.startswith('.'):
            raise RuntimeError('{}: file extension without dot'.format(filename))
        extension = file_extension[1:]
    try:
        h = backends[extension]
    except KeyError as ex:
        raise NotImplementedError("{}: unsupported extension".format(extension), ex)
    return h(filename, data)

def find_file(basename):
    """Find configuration file in any of the supported format.

BASENAME is the file name without extension."""
    default_filename = None
    for extension in backends:
        filename = '{}.{}'.format(basename, extension)
        if not default_filename:
            default_filename = filename
        if os.path.isfile(filename):
            return get_file(filename, extension=extension)
    return get_file(default_filename)

def supported_extensions():
    return [ ext for ext in backends ]

try:
    import yaml

    class YamlBackend(Backend):

        extension = 'yaml'

        def load_stream(self, fh):
            return yaml.load(fh, Loader=yaml.SafeLoader)

        def dump_stream(self, fh, data):
            yaml.dump(data, fh, default_flow_style=False)

    if not default_extension:
        default_extension = YamlBackend.extension
    backends[YamlBackend.extension] = YamlBackend
except ImportError:
    pass

try:
    import json

    class JsonBackend(Backend):

        extension = 'json'

        def load_stream(self, fh):
            return json.load(fh)

        def dump_stream(self, fh, data):
            json.dump(data, fh, indent=2)

    if not default_extension:
        default_extension = JsonBackend.extension
    backends[JsonBackend.extension] = JsonBackend
except ImportError:
    pass


