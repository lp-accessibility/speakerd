# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd (at) circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import io
import os
import shutil
import tempfile
import time
import unittest

import speakerd.config

from speakerd.output.cache import *

def create_file(path, data):
    with io.open(path, 'wb') as outfh:
        outfh.write(data)

def read_file(path):
    with io.open(path, 'rb') as infh:
        return infh.read()

class BaseTestCache(unittest.TestCase):

    def new_populated_cache(self, working_dir, max_entries, number_of_files):
        cache = self.new_cache(working_dir, max_entries)
        output_names = []
        for i in range(number_of_files):
            input_data = 'input data{}'.format(i).encode('ASCII')
            item = cache.get(input_data)
            self.assertIsNone(item.timestamp)
            output_names.append(os.path.basename(item.path))
            output_data = 'output data{}'.format(i).encode('ASCII')
            create_file(item.path, output_data)
            item.update()
            time.sleep(0.05)
        return cache, output_names

class TestDirectCache(BaseTestCache):

    def new_cache(self, working_dir, max_entries):
        return DirectOutputCache(working_dir, max_entries)

    def test_cache(self):
        """Cache one file."""
        with tempfile.TemporaryDirectory() as working_dir:
            cache = self.new_cache(working_dir, 3)
            input_data = b'first input'
            item = cache.get(input_data)
            self.assertIsNone(item.timestamp)
            item = cache.get(input_data)
            self.assertIsNone(item.timestamp)
            if item.is_cacheable():
                output_data = b'first output'
                create_file(item.path, output_data)
                item.update()
            item = cache.get(input_data)
            self.assertIsNotNone(item.timestamp)
            self.assertNotEqual(item.size, 0)
            output_data_from_cache = read_file(item.path)
            self.assertEqual(output_data, output_data_from_cache)

    def test_cache_cleanup(self):
        """Cache cleanup."""
        with tempfile.TemporaryDirectory() as working_dir:
            max_entries = 3
            number_of_files = max_entries + 2
            cache, output_names = self.new_populated_cache(working_dir, max_entries, number_of_files)
            remaining_names = sorted(output_names[number_of_files - max_entries:])
            cache.cleanup()
            current_names = sorted(os.listdir(working_dir))
            self.assertEqual(current_names, remaining_names)

    def test_cache_on_reload(self):
        """Cache cleanup on reload."""
        with tempfile.TemporaryDirectory() as working_dir:
            max_entries = 3
            number_of_files = max_entries + 2
            old_cache, output_names = self.new_populated_cache(working_dir, max_entries, number_of_files)
            remaining_names = sorted(output_names[number_of_files - max_entries:])
            cache = self.new_cache(working_dir, max_entries)
            cache.cleanup()
            current_names = sorted(os.listdir(working_dir))
            self.assertEqual(current_names, remaining_names)


class TestDelayedCache(BaseTestCache):

    def new_cache(self, working_dir, max_entries):
        return DelayedOutputCache(working_dir, max_entries)

    def test_cache_once(self):
        """Cache once."""
        with tempfile.TemporaryDirectory() as working_dir:
            cache = self.new_cache(working_dir, 3)
            input_data = b'one'
            item = cache.get(input_data)
            self.assertEqual(os.path.dirname(item.path), working_dir)
            self.assertIsNone(item.timestamp)
            self.assertEqual(item.size, 0)

    def test_cache_twice(self):
        """Cache twice."""
        with tempfile.TemporaryDirectory() as working_dir:
            cache = self.new_cache(working_dir, 3)
            input_data = b'one'
            item = cache.get(input_data)
            self.assertIsNone(item.timestamp)
            item = cache.get(input_data)
            self.assertIsNotNone(item.timestamp)
            self.assertEqual(item.size, 0)
            self.assertTrue(os.path.isfile(item.path))

    def test_cache_with_output(self):
        """Cache on the second call."""
        with tempfile.TemporaryDirectory() as working_dir:
            cache = self.new_cache(working_dir, 3)
            input_data = b'first input'
            item = cache.get(input_data)
            self.assertIsNone(item.timestamp)
            item = cache.get(input_data)
            self.assertIsNotNone(item.timestamp)
            self.assertEqual(item.size, 0)
            self.assertTrue(os.path.isfile(item.path))
            output_data = b'first output'
            create_file(item.path, output_data)
            item.update()
            item = cache.get(input_data)
            self.assertIsNotNone(item.timestamp)
            self.assertNotEqual(item.size, 0)
            output_data_from_cache = read_file(item.path)
            self.assertEqual(output_data, output_data_from_cache)

    def test_cache_cleanup(self):
        """Cache cleanup."""
        with tempfile.TemporaryDirectory() as working_dir:
            max_entries = 3
            number_of_files = max_entries + 2
            cache, output_names = self.new_populated_cache(working_dir, max_entries, number_of_files)
            remaining_names = sorted(output_names[number_of_files - max_entries:])
            cache.cleanup()
            current_names = sorted(os.listdir(working_dir))
            self.assertEqual(current_names, remaining_names)

if __name__ == '__main__':
    unittest.main()
