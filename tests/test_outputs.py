# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd (at) circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import os
import unittest

import speakerd.output

def executable_not_found(executable):
    for dirname in os.environ['PATH'].split(os.pathsep):
        if os.path.exists(os.path.join(dirname, executable)):
            return False
    return True

class TestOutputs(unittest.TestCase):

    def internal_test_output(self, name):
        config = speakerd.config.ConfigData({ 'cache': { 'directory': '/tmp', 'max_entries': 10 } })
        outputs = speakerd.output.available_outputs(config)
        self.assertIn(name, outputs)

    def test_dummy_output(self):
        """Check dummy output."""
        self.internal_test_output('dummy')

    @unittest.skipIf(executable_not_found('espeak'), "espeak not available")
    def test_espeak_output(self):
        """Check espeak output."""
        self.internal_test_output('espeak')

    @unittest.skipIf(executable_not_found('pico2wave'), "pico2wave not available")
    def test_pico_output(self):
        """Check pico output."""
        self.internal_test_output('pico')

if __name__ == '__main__':
    unittest.main()
