# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd (at) circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import logging
import os
import select
import shutil
import socket
import tempfile
import threading
import unittest

import speakerd.server
import speakerd.config

class PingPongClientHandler:

    def __init__(self):
        self.clients = {}

    def handle(self, client_number, rfile, wfile):
        self.clients[client_number] = True
        logging.debug('client #{}: reading data'.format(client_number))
        command = rfile.read()
        logging.debug('client #{}: read: {}'.format(client_number, command))
        answer = command.replace(b'PING', b'PONG')
        logging.debug('client #{}: answering: {}'.format(client_number, answer))
        wfile.write(answer)
        wfile.flush()
        return True

    def proceed(self):
        pass

    def detach(self, client_number):
        logging.debug('client #{}: detaching'.format(client_number))
        del self.clients[client_number]

class TestServer(unittest.TestCase):

    logger = None

    logging_level = logging.ERROR

    def setUp(self):
        self.working_dir = tempfile.mkdtemp()
        self.logger = logging.getLogger()
        hdlr = logging.StreamHandler()
        fmt = '%(levelname)s: %(message)s'
        hdlr.setFormatter(logging.Formatter(fmt))
        self.logger.addHandler(hdlr)
        self.logger.setLevel(self.logging_level)

    def tearDown(self):
        if self.logger:
            for hdlr in [ hdlr for hdlr in self.logger.handlers ]:
                hdlr.close()
                self.logger.removeHandler(hdlr)
        shutil.rmtree(self.working_dir)

    def internal_test_start_stop(self, config):
        client_handler = PingPongClientHandler()
        with speakerd.server.Server(config, client_handler) as server:
            thread = threading.Thread(target=server.run)
            self.assertFalse(server.started)
            self.assertFalse(server.done)
            thread.start()
            server.wait()
            self.assertTrue(server.started)
            server.interrupt()
            thread.join()
            self.assertFalse(server.started)

    def test_start_stop_local(self):
        """Start and stop a local server."""
        socket_path = os.path.join(self.working_dir, 'test.sock')
        config = speakerd.config.ConfigData({ 'socket_path': socket_path, 'max_clients': 2 })
        self.internal_test_start_stop(config)

    def test_start_stop_network(self):
        """Start and stop a network server."""
        config = speakerd.config.ConfigData({ 'host': 'localhost', 'port': 12345, 'max_clients': 2 })
        self.internal_test_start_stop(config)

    def test_single_client(self):
        """Single client."""
        socket_path = os.path.join(self.working_dir, 'test.sock')
        client_handler = PingPongClientHandler()
        socket_path = os.path.join(self.working_dir, 'test.sock')
        config = speakerd.config.ConfigData({ 'socket_path': socket_path, 'max_clients': 2 })
        with speakerd.server.Server(config, client_handler) as server:
            server.select_timeout = 3
            thread = threading.Thread(target=server.run)
            thread.start()
            server.wait()
            try:
                sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
                sock.setblocking(False)
                sock.connect(socket_path)
                with sock:
                    self.logger.debug('writing request')
                    sock.sendall(b'PING 1\r\n')
                    self.logger.debug('waiting for client answer')
                    poll = select.poll()
                    poll.register(sock, select.POLLIN | select.POLLPRI | select.POLLERR | select.POLLHUP | select.POLLNVAL)
                    data = None
                    while not data:
                        events = poll.poll(3000)
                        self.assertNotEqual(len(events), 0, "no reply from client")
                        if not events:
                            break
                        self.logger.debug('received {} events'.format(len(events)))
                        for evt in events:
                            self.logger.debug('event {} from {}'.format(evt[1], evt[0]))
                            if evt[1] == select.POLLIN:
                                self.logger.debug('reading data')
                                data = sock.recv(1024)
                                self.logger.debug('read: {}'.format(data))
                                self.assertEqual(data, b'PONG 1\r\n')
                                self.assertEqual(len(client_handler.clients), 1)
            finally:
                server.interrupt()
            thread.join()
        self.assertEqual(len(client_handler.clients), 0)

if __name__ == '__main__':
    unittest.main()
