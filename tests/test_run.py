# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd (at) circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import io
import os
import shutil
import sys
import tempfile
import unittest

import speakerd

def read_text_file(filename):
    with io.open(filename) as infh:
        return infh.read()

def env_var_exists(varname):
    return varname in os.environ

class TestRun(unittest.TestCase):

    def setUp(self):
        self.working_dir = tempfile.mkdtemp()
        self.dirs = {}
        self.options = []
        for name in ['config', 'cache', 'runtime']:
            dirname = os.path.join(self.working_dir, name)
            self.dirs[name] = dirname
            os.mkdir(dirname)
            self.options.append("--{}-dir={}".format(name, dirname))

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    @unittest.skipUnless(env_var_exists('XDG_RUNTIME_DIR'), "XDG not available")
    def test_generate_config(self):
        """Generate configuration."""
        args = self.options + [ "--exit" ]
        exit_code = speakerd.run(args)
        self.assertEqual(exit_code, 0)
        for name in ['main', 'logging']:
            config_file = os.path.join(self.dirs['config'], '{}.yaml'.format(name))
            self.assertTrue(os.path.exists(config_file), "{}: file not found".format(config_file))
        exit_code = speakerd.run(args)
        self.assertEqual(exit_code, 0)

if __name__ == '__main__':
    unittest.main()
