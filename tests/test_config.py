# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd (at) circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import io
import json
import os
import shutil
import sys
import tempfile
import unittest
import yaml

import speakerd.config

def write_text_file(filename, content):
    with io.open(filename, 'w') as outfh:
        outfh.write(content)

def read_text_file(filename):
    with io.open(filename) as infh:
        return infh.read()


CFG_SAMPLE = { 'value': 1, 'dict': { 'key': 'value' } }

YAML_SAMPLE = """---
value: 1
dict:
  key: value
"""

JSON_SAMPLE = '{"value": 1, "dict": {"key": "value"}}'

class TestConfigData(unittest.TestCase):

    def test_set_value(self):
        """Set values in config."""
        cfg = speakerd.config.ConfigData()
        cfg.set('section', 'key1', 'value1')
        cfg.set('section', 'key2', 'value2')
        value1 = cfg.get('section', 'key1')
        self.assertEqual(value1, 'value1')
        value2 = cfg.get('section', 'key2')
        self.assertEqual(value2, 'value2')

    def test_get_value(self):
        """Get values in config."""
        cfg = speakerd.config.ConfigData({ 'section': { 'key': 'value' }})
        self.assertEqual(cfg.get('section', 'key'), 'value')
        self.assertEqual(cfg.get('section', 'nokey'), None)
        self.assertEqual(cfg.get('section', 'nokey', defval='default'), 'default')
        self.assertEqual(cfg.get('nosection', 'nokey'), None)
        self.assertEqual(cfg.get('nosection', 'nokey', defval='default'), 'default')

    def test_get_sub_config(self):
        """Get configuration sub-tree."""
        cfg = speakerd.config.ConfigData({ 'section': {'subsection': { 'key': 'value' }}})
        subcfg = cfg.get('section')
        self.assertIsInstance(subcfg, speakerd.config.ConfigData)
        self.assertEqual(subcfg.get('subsection', 'key'), 'value')


class TestYaml(unittest.TestCase):

    def setUp(self):
        self.working_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    def test_yaml_read(self):
        """Read YAML configuration."""
        filename = os.path.join(self.working_dir, 'read.yaml')
        write_text_file(filename, YAML_SAMPLE)
        backend = speakerd.config.YamlBackend(filename)
        cfg = backend.load()
        self.assertEqual(cfg.get('value'), 1)
        self.assertEqual(cfg.get('dict', 'key'), 'value')

    def test_yaml_write(self):
        """Write YAML configuration."""
        filename = os.path.join(self.working_dir, 'write.yaml')
        backend = speakerd.config.YamlBackend(filename, CFG_SAMPLE)
        backend.save()
        with io.open(filename) as infh:
            cfg = yaml.load(infh, Loader=yaml.SafeLoader)
            self.assertIsInstance(cfg, dict)
            self.assertDictEqual(cfg, CFG_SAMPLE)

class TestJson(unittest.TestCase):

    def setUp(self):
        self.working_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    def test_json_read(self):
        """Read JSON configuration."""
        filename = os.path.join(self.working_dir, 'read.json')
        write_text_file(filename, JSON_SAMPLE)
        backend = speakerd.config.JsonBackend(filename)
        cfg = backend.load()
        self.assertEqual(cfg.get('value'), 1)
        self.assertEqual(cfg.get('dict', 'key'), 'value')

    def test_json_write(self):
        """Write JSON configuration."""
        filename = os.path.join(self.working_dir, 'write.json')
        backend = speakerd.config.JsonBackend(filename, CFG_SAMPLE)
        backend.save()
        with io.open(filename) as infh:
            cfg = json.load(infh)
            self.assertIsInstance(cfg, dict)
            self.assertDictEqual(cfg, CFG_SAMPLE)

class TestConfig(unittest.TestCase):

    def setUp(self):
        self.working_dir = tempfile.mkdtemp()
        self.basename = os.path.join(self.working_dir, 'config')
        for content, extension in [ (JSON_SAMPLE, 'json'), (YAML_SAMPLE, 'yaml') ]:
            filename = '{}.{}'.format(self.basename, extension)
            write_text_file(filename, content)

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    def test_supported_extensions(self):
        """Supported extensions."""
        supported_extensions = [ x for x in speakerd.config.backends ]
        self.assertEqual(supported_extensions, ['yaml', 'json'])

    def test_get_config(self):
        """Get configuration file."""
        for extension in [ 'yaml', 'json' ]:
            filename = '{}.{}'.format(self.basename, extension)
            config_file1 = speakerd.config.get_file(filename, extension=extension)
            self.assertTrue(config_file1.exists(), "{}: file not found".format(config_file1.filename))
            config_file2 = speakerd.config.get_file(filename)
            self.assertTrue(config_file2.exists(), "{}: file not found".format(config_file2.filename))
            self.assertEqual(config_file1.filename, config_file2.filename)

    def test_find_config(self):
        """Find configuration file in supported format."""
        config_file = speakerd.config.find_file(self.basename)
        self.assertTrue(config_file.exists(), "{}: file not found".format(config_file.filename))
        self.assertEqual(config_file.extension, speakerd.config.default_extension)
        cfg = config_file.load()
        self.assertEqual(cfg.get('value'), 1)
        self.assertEqual(cfg.get('dict', 'key'), 'value')

if __name__ == '__main__':
    unittest.main()
